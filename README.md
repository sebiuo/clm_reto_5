# Reto 5

ya instaldo el ingress-controller usando helm, se deben crear las credenciales 
para asegurar el ingreso a /private solo con usuario y password. Para realizar esta
tarea se crea un archivo con clave valor con la contraseña encriptada para el usuario user2.
```
htpasswd -c auth user2
```
Con el archivo auth creado se crea un secret para almacenar en el cluster las credenciales
```
kubectl create secret generic basic-auth --from-file=auth --namespace=desarrollo
```
Ahora se despliegan los manifiestos.
```
kubectl apply -f deployment.yaml --namespace=desarrollo
```
Para manejar el secret se crearon 2 archivos ingress que manejan el ingreso.
```
kubectl apply -f ingress.yaml --namespace=desarrollo
kubectl apply -f ingress-private.yaml --namespace=desarrollo
```
Se aplica la cuenta de servicios que utilizarán los pds y el emisor de certificados de seguridad y el horizontal autoscaler
```
kubectl apply -f service_account.yaml --namespace=desarrollo
kubectl apply -f ingress.yaml --namespace=desarrollo
kubectl apply -f has.yaml --namespace=desarrollo
```
Para probar las configuraciones:

Primero revisaremos que nos exija las credenciales para loguear.
```
curl https://clm-reto5.sebapi.ga/private
```
```
<html>
<head><title>401 Authorization Required</title></head>
<body>
<center><h1>401 Authorization Required</h1></center>
<hr><center>nginx</center>
</body>
</html>
```
Ahora probamos el resto de la app sin restricciones.
```
curl https://clm-reto5.sebapi.ga/ 
```
```
{"msg":"ApiRest prueba"}
```
